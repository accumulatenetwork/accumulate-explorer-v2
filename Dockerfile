FROM node:16-alpine as dependencies
WORKDIR /app
RUN apk add --no-cache autoconf libc6-compat libtool
RUN npm config set registry="http://registry.npmjs.org/"
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json
RUN npm ci

FROM node:16-alpine as builder
WORKDIR /app
COPY . .
COPY --from=dependencies /app/node_modules ./node_modules
RUN npm run build

FROM node:16-alpine as runner
WORKDIR /app
ENV NODE_ENV production

COPY --from=builder /app/next.config.js ./
COPY --from=builder /app/public ./public
COPY --from=builder /app/.next ./.next
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/package.json ./package.json

EXPOSE 3000
CMD ["npm", "run", "start"]