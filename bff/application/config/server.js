/* eslint-disable strict */
({
  host: '127.0.0.1',
  port: 8000,
  protocol: 'http',
  timeout: 5000,
  concurrency: 1000,
  queue: {
    size: 2000,
    timeout: 3000,
  },
  workers: {
    pool: 0,
    timeout: 3000,
  },
});
