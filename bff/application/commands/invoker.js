const { BaseCommand } = require('./base');

async function runCommand(command, { context, params }) {
  if (!BaseCommand.isPrototypeOf(command)) {
    throw new Error('Need define descendent of BaseCommand');
  }

  return new command({ context }).execution(params);
}

module.exports = { runCommand };
