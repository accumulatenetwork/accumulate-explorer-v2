'use strict';

const { BaseCommand } = require('../utils/base');

class MetricQueryCommand extends BaseCommand {
  constructor(args) {
    super(args);
  }
  async execution(data) {
    const { Client, logger } = this.context;
    return data;
  }

  static type = 'metric_query';
}

module.exports = MetricQueryCommand;
