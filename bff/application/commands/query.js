'use strict';

const { BaseCommand } = require('../utils/base');
class QueryCommand extends BaseCommand {
  constructor(args) {
    super(args);
  }
  async execution({ url }) {
    const {
      Client,
      logger,
      config: { accumulate },
    } = this.context;
    const client = new Client(accumulate);
    logger.log(`Query request for - ${url}`);

    const result = await client.queryUrl(url);
    logger.log(`Query result for - ${url}`);

    return handleResult(result, { client });
  }
  static type = 'query';
}

async function handleResult(result, { client }) {
  if (result.type === 'identity') {
    return strategyIdentity(result, { client });
  }

  return result;
}

async function strategyIdentity(result, { client }) {
  const keyBook = await client.queryUrl(result.data.keyBook);

  return dumpIdentity({ ...result, keyBookHash: keyBook.chainId });
}

function dumpIdentity(data) {
  return {
    type: data.type,
    text: data.data.url,
    textHash: data.chainId,
    keyBook: data.data.keyBook,
    keyBookHash: data.keyBookHash,
    managerKeyBook: data.data.managerKeyBook,
    mainChain: data.mainChain,
    merkleState: data.merkleState,
  };
}

function dump(result) {
  return {
    type: result.type,
    text: result.data.url,
    textHash: result.chainId,
    keyBook: result.data.keyBook,
    managerKeyBook: result.data.managerKeyBook,
    mainChain: result.mainChain,
    merkleState: result.merkleState,
  };
}

module.exports = QueryCommand;
