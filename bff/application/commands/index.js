'use strict';

const fs = require('fs');
const path = require('path');
const invoker = require('../utils/invoker');

const ignoredFiles = ['index.js'];
const commandsMap = {};

const getCommandByType = (type) => {
  const command = commandsMap[type];

  if (!command) {
    throw new Error(`No command for with type - ${type}`);
  }

  return command;
};

function commandsExports() {
  generateCommandsMap(__dirname, ignoredFiles);

  return { invoker, getCommandByType };
}

function generateCommandsMap(dirPath, ignoredFiles) {
  const files = fs.readdirSync(dirPath, { withFileTypes: true });

  for (const file of files) {
    if (ignoredFiles.includes(file.name)) {
      continue;
    }
    const filePath = path.join(dirPath, file.name);

    setCommandToMap(filePath);
  }
}

function setCommandToMap(filePath) {
  const command = require(filePath);

  if (!command) {
    return;
  }

  commandsMap[command.type] = command;
}

module.exports = commandsExports();
