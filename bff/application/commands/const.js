const COMMAND_TYPE = {
  metricQuery: 'metricQuery',
  query: 'query',
};

module.exports = { COMMAND_TYPE };
