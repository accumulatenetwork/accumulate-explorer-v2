'use strict';

const { BaseCommand } = require('../utils/base');

class CheckCommand extends BaseCommand {
  constructor(args) {
    super(args);
  }
  async execution({ url }) {
    const {
      Client,
      logger,
      config: { accumulate },
    } = this.context;
    const client = new Client(accumulate);

    const result = await client.queryUrl(url);

    return dump(result);
  }

  static type = 'check';
}

function dump(result) {
  return {
    type: result.type,
    chainId: result.chainId,
  };
}

module.exports = CheckCommand;
