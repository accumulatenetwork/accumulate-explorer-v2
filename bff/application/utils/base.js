'use strict';

class BaseCommand {
  constructor(args) {
    if (!args) {
      throw new Error('CONTEXT_REQUIRED');
    }

    this.context = args.context;
  }

  execution() {
    throw new Error(`DEFINE_EXECUTION - ${this.constructor.name}`);
  }

  static getType() {
    return this.type || this.name.toLowerCase();
  }
}

module.exports = { BaseCommand };
