'use strict';

const path = require('path');
const application = require('./lib/application');
const Server = require('./lib/server');
const Logger = require('./lib/logger');

process.env['NODE_CONFIG_DIR'] = __dirname + '/application/config/';
const config = require('config');

(async () => {
  const logPath = path.join(application.root, 'log');
  const logger = await new Logger(logPath);

  application.logger = logger;
  application.config = config;

  const logError = (err) => {
    logger.error(err ? err.stack : 'No exception stack available');
  };

  process.on('uncaughtException', logError);
  process.on('warning', logError);
  process.on('unhandledRejection', logError);

  application.server = new Server(config.server, application);

  await application.init();
})();
