'use strict';

const http = require('http');
const https = require('https');
const { HttpChannel } = require('./channel/http');
const { timeout, Semaphore, receiveBody, jsonParse } = require('metautil');

const SHUTDOWN_TIMEOUT = 5000;

class Server {
  constructor(config, application) {
    this.config = config;
    this.application = application;

    const { host, protocol, port, concurrency, queue } = config;

    this.port = port;
    this.protocol = protocol;
    this.host = host;
    this.semaphore = new Semaphore(concurrency, queue.size, queue.timeout);

    const transport = protocol === 'http' || this.balancer ? http : https;

    this.server = transport.createServer(this.listener.bind(this));
    this.server.listen(this.port, () => {
      this.application.logger.log(` server started on port ${this.port}`);
    });
  }

  async listener(req, res) {
    const { url } = req;

    const channel = new HttpChannel(this.application, req, res);

    if (url.startsWith('/api')) {
      this.request(channel);
    } else {
      channel.error(404);
    }
  }

  async request(channel) {
    const { req } = channel;
    const { url, method } = req;
    console.log(url, method);
    if (method === 'OPTIONS') {
      channel.options();
      return;
    }

    if (method !== 'POST') {
      channel.error(403);
      return;
    }

    const body = receiveBody(req);

    body
      .then((data) => {
        channel.message(data);
      })
      .catch((error) => {
        channel.error(500, { error });
      });
  }

  async close() {
    this.server.close((err) => {
      if (err) this.application.logger.error(err.stack);
    });

    await timeout(SHUTDOWN_TIMEOUT);
  }
}

module.exports = Server;
