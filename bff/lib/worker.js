'use strict';

const { parentPort, threadId, workerData } = require('worker_threads');
const { Client, LiteAccount, Keypair } = require('accumulate.js');
const { invoker, getCommandByType } = require('../application/commands');
const Logger = require('./logger');

const { logPath, config } = workerData;
const logger = new Logger(logPath, threadId);

parentPort.on('message', async ({ id, type, args }) => {
  logger.log(`From ${threadId} taskId-${id}`);

  try {
    const command = getCommandByType(type);

    const result = await invoker.runCommand(command, {
      context: { logger, config, Client },
      params: args,
    });
    parentPort.postMessage({ id, result, error: null });
  } catch (error) {
    logger.error(threadId, error);
    parentPort.postMessage({ id, result: null, error });
  }
});
