'use strict';

const metautil = require('metautil');
const http = require('http');

class Channel {
  constructor(application, req, res) {
    this.application = application;
    this.req = req;
    this.res = res;
    // this.ip = req.socket.remoteAddress;
  }

  async message(data) {
    const body = metautil.jsonParse(data);

    if (!body) {
      const error = new Error('JSON parsing error');
      this.error(400, { error, pass: true });
    }

    if (!body.type) {
      const error = new Error('Packet structure error');
      this.error(400, { error, pass: true });
    }

    this.rpc(body);
  }

  async rpc(args) {
    const { application } = this;

    const { result, error } = await application.emit(args);

    if (error) {
      this.error(404, { error, pass: true });
      return;
    }

    this.send(result);
  }

  error(code, { error = null, pass = false } = {}) {
    const { req, ip, application } = this;
    const { url, method } = req;

    const status = http.STATUS_CODES[code];
    const message = pass ? error.message : status || 'Unknown error';

    const httpCode = status ? code : 500;

    const reason = `${httpCode}\t${error ? error.stack : status}`;

    application.logger.error(`${ip}\t${method}\t${url}\t${reason}`);

    const packet = { error: { message, code } };
    this.send(packet, httpCode);
  }

  send() {
    throw new Error('You need implement send method');
  }
}

module.exports = Channel;
