const Channel = require('./channel');

const MIME_TYPES = {
  html: 'text/html; charset=UTF-8',
  json: 'application/json; charset=UTF-8',
  js: 'application/javascript; charset=UTF-8',
  css: 'text/css',
  png: 'image/png',
  ico: 'image/x-icon',
  svg: 'image/svg+xml',
};

const HEADERS = {
  'X-XSS-Protection': '1; mode=block',
  'X-Content-Type-Options': 'nosniff',
  'Strict-Transport-Security': 'max-age=31536000; includeSubdomains; preload',
  'Access-Control-Allow-Origin': '*',
  'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
  'Access-Control-Allow-Headers': 'Content-Type',
};

class HttpChannel extends Channel {
  constructor(application, req, res) {
    super(application, req, res);
  }

  send(obj, httpCode = 200) {
    const data = JSON.stringify(obj);
    this.write(data, httpCode, 'json');
  }

  write(data, httpCode, ext = 'json') {
    const { res } = this;
    if (res.writableEnded) return;

    const mimeType = MIME_TYPES[ext];
    res.writeHead(httpCode, { ...HEADERS, 'Content-Type': mimeType });
    res.end(data);
  }

  options() {
    const { res } = this;

    if (res.headersSend) {
      return;
    }

    res.writeHead(200, HEADERS);
    res.end();
  }
}

module.exports = { HttpChannel };
