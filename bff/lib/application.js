'use strict';

const path = require('path');
const { sample } = require('metautil');
const { Worker } = require('worker_threads');

const threads = [];
let active = 0;
const count = 2;

class Application {
  constructor() {
    this.config = null;
    this.logger = null;
    this.finalization = false;
    this.root = process.cwd();
    this.path = path.join(this.root, 'application');
    this.id = 0;
    this.tasks = new Map();
  }

  async init() {
    for (let id = 0; id < count; id++) {
      this.startWorker(id);
    }
  }

  startWorker(id) {
    const worker = new Worker(path.join(this.root, './lib/worker.js'), {
      workerData: {
        logPath: path.join(this.root, 'log'),
        config: this.config,
      },
    });
    threads[id] = worker;

    worker
      .on('exit', (code) => {
        if (code !== 0) this.startWorker(id);
        else if (--active === 0) process.exit(0);
      })
      .on('message', ({ id, result, error }) => {
        if (!this.tasks.has(id)) {
          return;
        }
        // TODO add time limit for response
        const { resolve } = this.tasks.get(id);

        resolve({ result, error });
      });
  }

  async stopWorkers() {
    for (const worker of threads) {
      worker.postMessage({ name: 'stop' });
    }
  }

  async shutdown() {
    this.finalization = true;
    if (this.server) await this.server.close();
    await this.stopWorkers();
    await this.logger.close();
  }

  emit({ type, ...args }) {
    return new Promise((resolve, reject) => {
      const id = ++this.id;

      this.tasks.set(id, { resolve, reject });
      const tread = randomTreadStrategy(threads);

      tread.postMessage({ id, type, args });
    });
  }
}

module.exports = new Application();

function randomTreadStrategy(threads) {
  return sample(threads);
}
