import { useRouter } from 'next/router';

export const withRouter = (Component) => (props) => {
  const router = useRouter();

  return <Component {...props} router={router} />;
};
