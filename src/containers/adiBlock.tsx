import { useAppDispatch, useAppSelector } from '@/hooks';
import { useRouter } from 'next/router';
import { selectAdi } from '@/features/adi/selectors';
import { fetchAdiData, resetAdiData } from '@/features/adi/actions';
import { useEffect } from 'react';

export const withADIBlock = (Component) => (props) => {
  const { pending, error, data } = useAppSelector(selectAdi);
  const dispatch = useAppDispatch();

  const {
    query: { adi },
  } = useRouter();

  const fetchData = () =>
    dispatch(fetchAdiData(decodeURIComponent(adi as string)));
  const resetData = () => dispatch(resetAdiData());

  useEffect(() => {
    fetchData();

    return () => {
      resetData();
    };
  }, [adi]);

  return (
    <Component
      {...props}
      adi={{ pending, error, data, fetchData, resetData }}
    />
  );
};
