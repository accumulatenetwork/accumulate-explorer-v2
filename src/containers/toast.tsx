import { toast as toastify } from 'react-toastify';
import { Content, CloseBtn } from '@/components/molecules/toasts';

import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

type toastParams = {
  type: 'info' | 'error';
  message: string;
};

export const toast = ({ type, message }: toastParams) => {
  if (type === 'error') {
    return <></>;
  }

  if (type === 'info') {
    return toastify.info(<Content message={message} />, {
      icon: () => (
        <InfoOutlinedIcon viewBox="2 2 20 20" sx={{ width: 16, height: 16 }} />
      ),
      closeButton: CloseBtn,
      theme: 'colored',
    });
  }
};

export const withToast = (Component) => (props) => {
  return <Component {...props} toast={toast} />;
};
