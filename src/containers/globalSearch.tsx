import { useAppDispatch, useAppSelector } from '@/hooks';
import { selectSearch } from '@/features/search/selectors';
import {
  fetchData,
  setValue as setSearchValue,
} from '@/features/search/actions';

export const withGlobalSearch = (Component) => (props) => {
  const { value, pending, error, data } = useAppSelector(selectSearch);
  const dispatch = useAppDispatch();

  const setValue = (value: string) => {
    dispatch(setSearchValue(value));
  };

  const onRequest = (value: string) => {
    dispatch(fetchData(value));
  };

  return (
    <Component
      {...props}
      search={{ value, pending, error, data, setValue, onRequest }}
    />
  );
};
