import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchData = createAsyncThunk(
  'search/data/fetch',
  async (url: string) => {
    const response = await axios.post(process.env.NEXT_PUBLIC_BFF_API, {
      type: 'check',
      url,
    });

    return response.data;
  },
);

export const setValue = createAction<string>('search/value/set');

export const resetData = createAction<void>('search/data/reset');
