import { createReducer } from '@reduxjs/toolkit';
import { fetchData, setValue, resetData } from './actions';

export type SearchState = {
  value: string;
  data: Record<string, unknown>;
  pending: boolean;
  error: boolean;
};

const initialState: SearchState = {
  value: '',
  data: null,
  pending: false,
  error: false,
};

const searchReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(fetchData.pending, (state) => {
      state.pending = true;
      state.error = false;
    })
    .addCase(fetchData.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.data = payload;
    })
    .addCase(fetchData.rejected, (state) => {
      state.pending = false;
      state.error = true;
    })
    .addCase(setValue, (state, { payload }) => {
      state.value = payload;
    })
    .addCase(resetData, (state) => {
      state = { value: state.value, error: false, pending: false, data: null };
    });
});

export default searchReducer;
