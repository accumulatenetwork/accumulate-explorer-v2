import { createReducer } from '@reduxjs/toolkit';
import { fetchAdiData, resetAdiData } from './actions';

export type AdiState = {
  data: Record<string, unknown>;
  pending: boolean;
  error: boolean;
};

const initialState: AdiState = {
  data: null,
  pending: false,
  error: false,
};

const searchReducer = createReducer(initialState, (builder) => {
  builder
    .addCase(fetchAdiData.pending, (state) => {
      state.pending = true;
    })
    .addCase(fetchAdiData.fulfilled, (state, { payload }) => {
      state.pending = false;
      state.data = payload;
    })
    .addCase(fetchAdiData.rejected, (state) => {
      state.pending = false;
      state.error = true;
    })
    .addCase(resetAdiData, (state) => {
      state.pending = false;
      state.error = true;
      state.data = null;
    });
});

export default searchReducer;
