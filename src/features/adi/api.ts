import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

type ADI = any;

export const api = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: process.env.NEXT_PUBLIC_BFF_API }),
  tagTypes: ['ADI'],
  endpoints: (build) => ({
    getAdiBlock: build.mutation<ADI, Partial<ADI>>({
      query: (body) => ({
        url: `api`,
        method: 'POST',
        body,
      }),
      invalidatesTags: [{ type: 'ADI', id: 'LIST' }],
    }),
  }),
});

export const { useGetAdiBlockMutation } = api;
