import { createAsyncThunk, createAction } from '@reduxjs/toolkit';
import axios from 'axios';

export const fetchAdiData = createAsyncThunk(
  'adi/data',
  async (url: string /* thunkApi */) => {
    const response = await axios.post(process.env.NEXT_PUBLIC_BFF_API, {
      type: 'query',
      url,
      // signal: thunkApi.signal
    });

    return response.data;
  },
);

export const resetAdiData = createAction<void>('search/data/reset');
