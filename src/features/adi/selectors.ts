import { createSelector } from '@reduxjs/toolkit';
import type { RootState } from '../../store';

export const selectAdi = (state: RootState) => state.adi;

export const adiSelector = createSelector(selectAdi, (state) => state);
