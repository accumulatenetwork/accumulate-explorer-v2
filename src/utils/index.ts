type navigatePages = 'account/token' | 'adi' | 'keybook' | 'page' | 'token' | 'tx';

export const navigateTo = (page: navigatePages, adi: string): string => {
  return `/${page}/${encodeURIComponent(adi)}`;
};
