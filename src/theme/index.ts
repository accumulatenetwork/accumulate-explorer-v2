import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  typography: {
    fontFamily: [
      'Montserrat',
      'Open Sans',
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      'Helvetica',
      'Arial',
      'sans-serif',
    ].join(','),

    link: {
      fontFamily: [
        'Montserrat',
        'Open Sans',
        '-apple-system',
        'BlinkMacSystemFont',
        '"Segoe UI"',
        'Roboto',
        'Helvetica',
        'Arial',
        'sans-serif',
      ].join(','),
      fontWeight: 400,
      fontSize: 16,
    },

    h2: {
      fontSize: '1.5rem',
      fontWeight: 500,
    },
    h5: {
      fontSize: '1.375rem',
      fontWeight: 500,
    },
    h6: {
      fontSize: '1.125rem',
      fontWeight: 500,
    },
    body2: {
      fontSize: '0.8125rem',
      fontWeight: 600,
    },
    listText: {
      fontSize: '0.875rem',
      fontWeight: 500,
    },
    hint: {
      fontSize: '0.6875rem',
      fontWeight: 500,
      lineHeight: 13,
    },
  },
  components: {
    MuiListItemText: {
      styleOverrides: {
        primary: {
          fontSize: '0.75rem',
          fontWeight: 500,
        },
      },
    },
  },
  palette: {
    primary: {
      main: '#00A6FB',
      contrastText: '#fff',
    },
    secondary: {
      main: '#011119',
    },
    black: {
      main: '#011119',
      dark: '000',
    },
    success: {
      main: '#87C200',
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 900,
      lg: 1200,
      xl: 1528,
    },
  },
});
