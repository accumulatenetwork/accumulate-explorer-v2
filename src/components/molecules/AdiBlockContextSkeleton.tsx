import { Skeleton, Divider } from '@mui/material';
import { Fragment } from 'react';

type AdiBlockContextSkeletonProps = {
  rows: { height: number; width: number }[];
};

export const AdiBlockContextSkeleton = ({
  rows = [],
}: AdiBlockContextSkeletonProps) => {
  return (
    <>
      {rows.map((row, index) => (
        <Fragment key={`${row.height + index}-index`}>
          {index === 0 ? '' : <Divider />}
          <Skeleton
            variant="rectangular"
            height={row.height}
            width={row.width}
          />
        </Fragment>
      ))}
    </>
  );
};
