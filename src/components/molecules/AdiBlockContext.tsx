import { Fragment } from 'react';
import { styled } from '@mui/material/styles';
import { List } from '@/components/atoms/list/list';
import {
  ItemContentDefaultProps,
  ListItemRow,
} from '@/components/molecules/listItemRow';
import { Divider } from '@mui/material';
import { withADIBlock } from '@/containers/adiBlock';

import { Any } from '@/interfaces';

export type AdiBlockContextProps = {
  rows: ({ title: string } & ItemContentDefaultProps[]) | unknown[];
};

export const AdiBlockContext = styled(
  ({ rows, ...props }: Any & AdiBlockContextProps) => {
    return (
      <List {...props} sx={{ width: '100%', bgcolor: 'background.paper' }}>
        {rows.map((row, index) => (
          <Fragment key={row.title}>
            {index === 0 ? '' : <Divider />}
            <ListItemRow data={row} />
          </Fragment>
        ))}
      </List>
    );
  },
)<AdiBlockContextProps>`
  padding-top: 0;
`;

export default withADIBlock(AdiBlockContext);
