import { styled } from '@mui/material/styles';
import { Typography } from '@mui/material';

export const Title = styled((props) => {
  return <Typography variant="h2" {...props} />;
})``;
