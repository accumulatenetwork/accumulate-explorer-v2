import { CancelIcon } from '@/components/atoms/cancelIcon';
import { styled } from '@mui/material/styles';

type closeBtnProps = {
  closeToast: () => void;
};

export const CloseBtnWrapper = styled('div')`
  display: flex;
  align-items: center;
  padding: 0 8px;
`;

export const CloseBtn = ({ closeToast }: closeBtnProps) => {
  return (
    <CloseBtnWrapper>
      <CancelIcon onClick={closeToast} />
    </CloseBtnWrapper>
  );
};
