import { Typography } from '@mui/material';

type InfoToasterProps = { message: string };

export const Content = ({ message }: InfoToasterProps) => {
  return <Typography variant="listText">{message}</Typography>;
};
