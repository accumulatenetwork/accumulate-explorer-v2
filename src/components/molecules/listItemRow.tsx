import { FC } from 'react';
import { styled } from '@mui/material/styles';
import Tooltip, { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';

import { ListItem } from '@/components/atoms/list/item';
import { ListItemIcon } from '@/components/atoms/list/itemIcon';
import { ListItemText } from '@/components/atoms/list/listItemText';
import { Link } from '@/components/atoms/link';

import { CopyText } from '@/components/molecules/CopyText';
import { CopyHash } from '@/components/molecules/CopyHash';

import { Typography } from '@mui/material';

import { Any, Children } from '@/interfaces';

export interface ItemContentDefaultProps extends Children {
  title: string;
  text: string;
  link: string;
  isTextCopy: boolean;
  hash: string;
  hint?: string;
}

export type ListItemRowProps = Children & {
  Icon?: FC;
  ItemContext?: FC;
  data: ItemContentDefaultProps;
};

const FlexWrapper = styled('div')`
  display: flex;
  align-items: center;

  .MuiSvgIcon-root {
    font-size: 16px;
  }

  ${CopyText}, ${CopyHash} {
    margin-left: 10px;
  }
`;

const ItemContentDefault = (data: ItemContentDefaultProps) => {
  return (
    <FlexWrapper>
      {data.link ? (
        <Link
          href={data.link}
          variant="listText"
          underline="none"
          color="primary"
        >
          {data.text}
        </Link>
      ) : (
        <Typography variant="listText">{data.text}</Typography>
      )}
      {data.isTextCopy ? <CopyText text={data.text} /> : null}
      <CopyHash hash={data.hash} />
    </FlexWrapper>
  );
};

export const ListItemRow = styled(
  ({
    Icon = InfoOutlinedIcon,
    ItemContext = ItemContentDefault,
    data,
    ...props
  }: Any & ListItemRowProps) => {
    return (
      <ListItem {...props}>
        <ListItemIcon>
          {data.hint && (
            <Tooltip title={data.hint}>
              <Icon />
            </Tooltip>
          )}
        </ListItemIcon>
        <ListItemText primary={data.title} />
        <ItemContext {...data} />
      </ListItem>
    );
  },
)<ListItemRowProps>`
  & .MuiListItem-root.MuiListItem-gutters {
    padding-left: 36px;
  }

  ${ListItemIcon} {
    min-width: 24px;
    margin-right: 23px;
    cursor: pointer;
  }

  & ${ListItemText} {
    width: 100%;
    max-width: 140px;
  }
`;
