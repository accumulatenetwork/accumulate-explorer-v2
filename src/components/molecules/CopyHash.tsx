import { FC, useState } from 'react';
import { styled } from '@mui/material/styles';
import Popper, { PopperPlacementType } from '@mui/material/Popper';
import { ClickAwayListener, Fade, Typography } from '@mui/material';

import { IconButton } from '@/components/atoms/iconButton';
import { HashIcon } from '@/components/atoms/hashIcon';
import { CopyText } from '@/components/molecules/CopyText';

import { Any } from '@/interfaces';

const PopperBody: FC = styled('div')`
  background: rgba(1, 17, 25, 0.8);
  box-shadow: 0px 3px 10px -1px rgba(0, 0, 0, 0.15);
  border-radius: 2px;
  padding: ${({ theme }) => `${theme.spacing(0.5)} ${theme.spacing(0.75)}`};
  display: flex;
  align-items: center;

  .MuiSvgIcon-root {
    font-size: 1rem;
  }

  ${CopyText} {
    margin-left: ${({ theme }) => theme.spacing(1)};
  }
`;

type CopyHashProps = { hash: string };

export const CopyHash = styled(({ hash, ...props }: Any & CopyHashProps) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const [open, setOpen] = useState(false);
  const [placement, setPlacement] = useState<PopperPlacementType>();

  const handleClick =
    (newPlacement: PopperPlacementType) =>
    (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
      setOpen((prev) => placement !== newPlacement || !prev);
      setPlacement(newPlacement);
    };

  const handleClickAway = () => {
    setOpen(false);
  };

  return (
    <ClickAwayListener onClickAway={handleClickAway}>
      <div {...props}>
        <IconButton
          aria-label="copy hash"
          size="small"
          color="primary"
          edge={false}
          disableRipple
          onClick={handleClick('top')}
        >
          <HashIcon />
        </IconButton>
        <Popper
          open={open}
          anchorEl={anchorEl}
          placement={placement}
          transition
        >
          {({ TransitionProps }) => (
            <Fade {...TransitionProps} timeout={350}>
              <PopperBody>
                <Typography
                  color="white"
                  component="span"
                  sx={{ fontSize: 11 }}
                >
                  The content of the Popper.
                </Typography>
                <CopyText text={hash} />
              </PopperBody>
            </Fade>
          )}
        </Popper>
      </div>
    </ClickAwayListener>
  );
})<CopyHashProps>`
  ${IconButton} {
    padding: 0;
    opacity: 0.5;
    transition: opacity 0.3s ease-out;

    &:hover,
    &.opened {
      opacity: 1;
    }
  }
`;
