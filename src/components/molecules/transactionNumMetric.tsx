import { Paper, PaperProps, Typography } from '@mui/material';
import { GraphicIcon } from '@/components/atoms/graphicIcon';
import { styled } from '@mui/material/styles';

const TextWrapper = styled('div')`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex: 1;
  margin-left: 24px;
`;

interface TransactionNumMetric extends PaperProps {
  amount: number | string;
}

export const TransactionNumMetric = styled(
  ({ amount, ...props }: TransactionNumMetric) => {
    return (
      <Paper {...props}>
        <GraphicIcon />
        <TextWrapper>
          <Typography variant="body2" color="#4A4A4A">
            TPS/1h
          </Typography>
          <Typography variant="h5">{amount}</Typography>
        </TextWrapper>
      </Paper>
    );
  },
)`
  width: 256px;
  display: flex;
  height: 56px;
  align-items: center;
  justify-content: space-between;
  padding: 14px;
`;
