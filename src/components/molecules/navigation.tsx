import { Link } from '@/components/atoms/link';
import { styled } from '@mui/material/styles';
import { useRouter } from 'next/router';

export const Navigation = styled(
  ({
    className = '',
    links = [],
  }: {
    className?: string;
    links?: {
      href: string;
      title: string;
    }[];
  }) => {
    const router = useRouter();

    return (
      <div className={className}>
        {links.map((link) => (
          <Link
            href={link.href}
            key={link.href + link.title}
            variant="link"
            underline="none"
            color="secondary"
            className={router.asPath === link.href ? 'active' : ''}
          >
            {link.title}
          </Link>
        ))}
      </div>
    );
  },
)`
  display: flex;
  flex: 1;
  margin-left: 24px;

  @keyframes border_anim {
    0% {
      width: 0%;
    }
    100% {
      width: 100%;
    }
  }

  & > .MuiLink-root {
    position: relative;
    margin: 0 24px;
    display: block;
    text-align: center;
    line-height: 1.25;
    font-weight: 600;

    &:before {
      content: '';
      position: absolute;
      left: 0;
      bottom: -2px;
      width: 0;
    }
    &:hover {
      &:before {
        animation: border_anim 0.3s linear forwards;
        border-bottom: solid 2px ${({ theme }) => theme.palette.primary.main};
      }
    }
    &.active {
      &:before {
        width: 100%;
        animation: none;
        border-bottom: solid 2px ${({ theme }) => theme.palette.primary.main};
      }
    }
  }
`;
