import { Paper } from '@mui/material';
import React from 'react';
import { styled } from '@mui/material/styles';
import CancelOutlinedIcon from '@mui/icons-material/CancelOutlined';

import { Button } from '@/components/atoms/button';
import { Input } from '@/components/atoms/input';
import { IconHideButton } from '@/components/atoms/iconHideButton';
import { SearchIcon } from '@/components/atoms/searchIcon';

interface SearchProps {
  placeholder: string;
  closeIcon?: JSX.Element;
  disabled?: boolean;
  searchIcon?: JSX.Element;
  style?: string;
  value?: string;
  onChange?: (event) => void;
  onKeyUp?: (event) => void;
  onFocus?: (event) => void;
  onBlur?: (event) => void;
  cancelOnEscape?: () => void;
  onCancelSearch?: () => void;
  onRequestSearch?: (event) => void;
  className?: string;
}

const InputContainer = styled('div')`
  flex: 1;
  display: flex;
  padding: 0 16px;
`;

export const Search = styled(
  ({
    className,
    closeIcon,
    disabled,
    cancelOnEscape,
    onCancelSearch,
    onRequestSearch,
    searchIcon,
    style,
    ...inputProps
  }: SearchProps) => {
    const inputRef = React.useRef<HTMLInputElement>();
    const [value, setValue] = React.useState(inputProps.value);

    React.useEffect(() => {
      setValue(inputProps.value);
    }, [inputProps.value]);

    const handleFocus = React.useCallback(
      (e) => {
        if (inputProps.onFocus) {
          inputProps.onFocus(e);
        }
      },
      [inputProps.onFocus],
    );

    const handleBlur = React.useCallback(
      (e) => {
        setValue((v) => v.trim());
        if (inputProps.onBlur) {
          inputProps.onBlur(e);
        }
      },
      [inputProps.onBlur],
    );

    const handleInput = React.useCallback(
      (e) => {
        setValue(e.target.value);

        if (inputProps.onChange) {
          inputProps.onChange(e.target.value);
        }
      },
      [inputProps.onChange],
    );

    const handleCancel = React.useCallback(() => {
      setValue('');
      if (onCancelSearch) {
        onCancelSearch();
      }
    }, [onCancelSearch]);

    const handleRequestSearch = React.useCallback(
      (event) => {
        event.preventDefault();
        if (onRequestSearch) {
          onRequestSearch(value);
        }
      },
      [onRequestSearch, value],
    );

    const handleKeyUp = React.useCallback(
      (e) => {
        if (e.charCode === 13 || e.key === 'Enter') {
          handleRequestSearch(e);
        } else if (
          cancelOnEscape &&
          (e.charCode === 27 || e.key === 'Escape')
        ) {
          handleCancel();
        }
        if (inputProps.onKeyUp) {
          inputProps.onKeyUp(e);
        }
      },
      [handleRequestSearch, cancelOnEscape, handleCancel, inputProps.onKeyUp],
    );

    return (
      <Paper
        className={className}
        component="form"
        onSubmit={handleRequestSearch}
      >
        <InputContainer>
          <Input
            {...inputProps}
            inputRef={inputRef}
            onBlur={handleBlur}
            value={value}
            onChange={handleInput}
            onKeyUp={handleKeyUp}
            onFocus={handleFocus}
            fullWidth
            disabled={disabled}
          />
          <IconHideButton
            aria-label="delete"
            size="small"
            hidden={!value}
            onClick={handleCancel}
            disabled={disabled}
          >
            {React.cloneElement(closeIcon, {
              fontSize: 'inherit',
              viewBox: '2 2 20 20',
            })}
          </IconHideButton>
        </InputContainer>
        <Button
          type="submit"
          variant="contained"
          aria-label="search"
          disabled={disabled}
        >
          {React.cloneElement(searchIcon, {})}
        </Button>
      </Paper>
    );
  },
)`
  display: flex;
  align-items: center;
  height: 50px;

  ${Button} {
    height: inherit;
    border-bottom-left-radius: 0;
    border-top-left-radius: 0;
  }

  ${Input} {
    font-size: 14px;

    & .MuiInputBase-input {
      padding: 0;
    }
  }
`;

Search.defaultProps = {
  closeIcon: <CancelOutlinedIcon />,
  disabled: false,
  placeholder: 'Search',
  searchIcon: <SearchIcon />,
  style: null,
  value: '',
  onChange: () => {},
  onKeyUp: () => {},
  onFocus: () => {},
  onBlur: () => {},
  cancelOnEscape: () => {},
  onCancelSearch: () => {},
  onRequestSearch: () => {},
};
