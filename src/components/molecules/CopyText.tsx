import { useState, useRef, MouseEvent } from 'react';
import { styled } from '@mui/material/styles';
import ContentCopyIcon from '@mui/icons-material/ContentCopy';
// import Popover from '@mui/material/Popover';
// import Typography from '@mui/material/Typography';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import { IconButton } from '@/components/atoms/iconButton';

type CopyTextProps = { text: string };

const timeout = 1000;

export const CopyText = styled(({ text, ...props }: CopyTextProps) => {
  const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
  const timer = useRef(null);
  // const open = Boolean(anchorEl);
  const copied = Boolean(anchorEl);

  const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
    navigator.clipboard.writeText(text);
    timer.current = setTimeout(handleClose, timeout);
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    if (!timer.current) {
      return;
    }

    clearTimeout(timer.current);
    setAnchorEl(null);
  };

  return (
    <div {...props}>
      <IconButton
        aria-label="copy"
        size="small"
        color={copied ? 'success' : 'primary'}
        edge={false}
        disableRipple
        onClick={handleClick}
        className={copied ? 'success' : ''}
      >
        {copied ? <CheckCircleOutlineIcon /> : <ContentCopyIcon />}
      </IconButton>
    </div>
  );
})`
  display: flex;
  align-items: center;

  ${IconButton} {
    padding: 0;
    opacity: 0.5;
    transition: opacity 0.3s ease-out;
    &:hover,
    &.success {
      opacity: 1;
    }
  }
`;
