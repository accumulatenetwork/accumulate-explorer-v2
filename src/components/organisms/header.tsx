import { styled } from '@mui/material/styles';
import { Container } from '@mui/material';
import { LogoIcon } from '@/components/atoms/logoIcon';
import { Link } from '@/components/atoms/link';
import { Navigation } from '@/components/molecules/navigation';

const HeaderWrapper = styled('div')`
  line-height: 0;
  padding-top: 18px;
  padding-bottom: 18px;
  display: flex;
  align-items: center;
  justify-content: flex-start;
`;

export const Header = () => {
  return (
    <Container maxWidth="xl">
      <HeaderWrapper>
        <Link href="/dashboard">
          <LogoIcon color="primary" />
        </Link>
        <Navigation
          links={[
            { href: '/dashboard/', title: 'Dashboard' },
            { href: '/page/', title: 'Dashboard2' },
          ]}
        />
      </HeaderWrapper>
    </Container>
  );
};
