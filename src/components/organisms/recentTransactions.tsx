import { styled } from '@mui/material/styles';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@/components/atoms/accordion';
import { Typography } from '@mui/material';
import { RoundedArrowRightIcon } from '@/components/atoms/roundedArrowRightIcon';

export const RecentTransactions = styled(() => {
  return (
    <Accordion expanded>
      <AccordionSummary expandIcon={<RoundedArrowRightIcon />}>
        <Typography variant="h6">Collapsible Group Item #1</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
          lacus ex, sit amet blandit leo lobortis eget.
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
})``;
