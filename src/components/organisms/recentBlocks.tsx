import { styled } from '@mui/material/styles';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@/components/atoms/accordion';
import { Typography } from '@mui/material';
import { CubIcon } from '@/components/atoms/cubIcon';

export const RecentBlocks = styled(() => {
  return (
    <Accordion expanded>
      <AccordionSummary expandIcon={<CubIcon />}>
        <Typography variant="h6">10 Most Recent Blocks</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
          lacus ex, sit amet blandit leo lobortis eget.
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
})``;
