import { styled } from '@mui/material/styles';
import { Title } from '@/components/molecules/title';
import { CopyText } from '@/components/molecules/CopyText';

type CopyTitleProps = { title: string };

const CopyTitleWrapper = styled('div')`
  margin-left: 12px;
  display: flex;
  align-items: center;
  ${Title} {
    margin-right: 10px;
  }
`;

export const CopyTitle = styled(({ title }: CopyTitleProps) => {
  return (
    <CopyTitleWrapper>
      <Title>{title}</Title>
      <CopyText text={title} />
    </CopyTitleWrapper>
  );
})``;
