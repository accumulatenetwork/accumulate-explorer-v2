import { useEffect } from 'react';

import { styled } from '@mui/material/styles';
import { Container } from '@mui/material';

import { Search } from '@/components/molecules/search';
import { colors } from '@/const/colors';

import { navigateTo } from '@/utils/index';

const DarkBackground = styled('div')`
  width: 100%;
  background: ${colors.dark[0]};
  padding: 30px 0;
`;

export const GlobalSearch = ({ toast, search, router }) => {
  useEffect(() => {
    if (search.error) {
      toast({
        type: 'info',
        message: 'Nothing was found',
      });
      return;
    }

    if (search.data) {
      router.push(navigateTo('adi', search.value));
    }
  }, [search.data, search.error]);

  return (
    <DarkBackground>
      <Container maxWidth="xl">
        <Search
          disabled={search.pending}
          onChange={search.setValue}
          onRequestSearch={search.onRequest}
          placeholder="Input a specific URL (ADI URL, Keybook #, Keypage # transaction ID, etc.) or search for ADIs and Tokens"
        />
      </Container>
    </DarkBackground>
  );
};
