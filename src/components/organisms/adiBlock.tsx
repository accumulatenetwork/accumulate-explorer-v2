import { styled } from '@mui/material/styles';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@/components/atoms/accordion';
import { Typography } from '@mui/material';
import { Children } from '@/interfaces';

export const AdiBlock = styled(({ children, ...props }: Children) => {
  return (
    <Accordion defaultExpanded {...props}>
      <AccordionSummary>
        <Typography variant="h6">ADI Information</Typography>
      </AccordionSummary>
      <AccordionDetails>{children}</AccordionDetails>
    </Accordion>
  );
})`
  ${AccordionSummary.toString()} .MuiAccordionSummary-expandIconWrapper.Mui-expanded {
    transform: rotate(90deg);
  }

  & .MuiAccordionSummary-content {
    margin-left: ${({ theme }) => theme.spacing(1.75)};
  }

  & ${AccordionDetails} {
    padding: 0;
  }
`;
