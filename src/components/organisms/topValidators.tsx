import { styled } from '@mui/material/styles';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@/components/atoms/accordion';
import { Typography } from '@mui/material';

import CheckCircleOutlineRoundedIcon from '@mui/icons-material/CheckCircleOutlineRounded';

export const TopValidators = styled((props) => {
  return (
    <Accordion expanded {...props}>
      <AccordionSummary
        expandIcon={<CheckCircleOutlineRoundedIcon viewBox="0 0 22 22" />}
      >
        <Typography variant="h6">Top 10 Validators</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
          lacus ex, sit amet blandit leo lobortis eget.
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
})`
  /* ${AccordionSummary} .MuiAccordionSummary-expandIconWrapper.Mui-expanded {
    transform: rotate(0);
  } */
`;
