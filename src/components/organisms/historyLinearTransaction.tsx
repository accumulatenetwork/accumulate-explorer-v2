import { styled } from '@mui/material/styles';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
} from '@/components/atoms/accordion';
import { Typography } from '@mui/material';
import { OppositeArrowsIcon } from '@/components/atoms/oppositeArrowsIcon';

export const HistoryLinearTransaction = styled(() => {
  return (
    <Accordion expanded>
      <AccordionSummary expandIcon={<OppositeArrowsIcon />}>
        <Typography variant="h6">14-Day Transaction History</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Typography>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
          malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
          lacus ex, sit amet blandit leo lobortis eget.
        </Typography>
      </AccordionDetails>
    </Accordion>
  );
})``;
