import { Container, ContainerProps, Grid } from '@mui/material';
import { styled } from '@mui/material/styles';
import { Title } from '@/components/molecules/title';

type TitleBlockProps = ContainerProps & { justify?: string };

export const TitleBlock = styled(
  ({
    children,
    title,
    justify = 'space-between',
    ...props
  }: TitleBlockProps) => {
    return (
      <Container maxWidth="xl" {...props}>
        <Grid container justifyContent={justify} alignItems="center">
          <Grid item>
            <Title>{title}</Title>
          </Grid>
          <Grid>{children}</Grid>
        </Grid>
      </Container>
    );
  },
)`
  padding-top: 20px;
  padding-bottom: 20px;
`;
