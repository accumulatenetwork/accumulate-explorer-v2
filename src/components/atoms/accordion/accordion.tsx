import { styled } from '@mui/material/styles';
import MuiAccordion, { AccordionProps } from '@mui/material/Accordion';

export const Accordion = styled((props: AccordionProps) => (
  <MuiAccordion disableGutters elevation={0} {...props} />
))`
  box-shadow: 0px 3px 10px -1px rgba(0, 0, 0, 0.15);
  &:not(:last-child) {
    border-bottom: 0;
  }
  &:before {
    display: 'none';
  }
  border-radius: '4px';
`;
