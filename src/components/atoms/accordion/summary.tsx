import { styled } from '@mui/material/styles';
import ArrowForwardIosSharpIcon from '@mui/icons-material/ArrowForwardIosSharp';
import MuiAccordionSummary, {
  AccordionSummaryProps,
} from '@mui/material/AccordionSummary';

export const AccordionSummary = styled((props: AccordionSummaryProps) => {
  return (
    <MuiAccordionSummary
      expandIcon={<ArrowForwardIosSharpIcon sx={{ fontSize: '0.9rem' }} />}
      {...props}
    />
  );
})`
  flex-direction: row-reverse;
  & .MuiAccordionSummary-expandIconWrapper.Mui-expanded {
    transform: rotate(0deg);
  }

  & .MuiAccordionSummary-expandIconWrapper {
    color: ${({ theme }) => theme.palette.primary.main};
  }

  & .MuiAccordionSummary-content {
    margin-left: ${({ theme }) => theme.spacing(2.5)};
  }
`;
