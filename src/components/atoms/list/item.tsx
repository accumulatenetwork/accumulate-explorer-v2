import { styled } from '@mui/material/styles';
import { ListItem as MuiListItem } from '@mui/material';
import { FC } from 'react';

export const ListItem: FC = styled(MuiListItem)``;
