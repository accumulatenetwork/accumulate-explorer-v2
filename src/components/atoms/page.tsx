import { styled } from '@mui/material/styles';
import { theme } from '@/theme';

export const Page = styled('div')`
  width: 100%;
  min-width: 360px;
  min-height: calc(100vh - 62px);
  padding: ${theme.spacing(2.5)}px;
  box-sizing: border-box;
`;
