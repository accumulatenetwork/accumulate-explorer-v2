import React from 'react';
import { SvgIcon } from '@mui/material';
import CubSvgIcon from '@/assets/cub.svg';

export const CubIcon = React.memo((props) => (
  <SvgIcon {...props} viewBox="0 0 24 28" component={CubSvgIcon} />
));
