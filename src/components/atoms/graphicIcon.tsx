import React from 'react';
import { SvgIcon } from '@mui/material';
import GraphicSvgIcon from '@/assets/graphic.svg';

export const GraphicIcon = React.memo((props) => (
  <SvgIcon {...props} viewBox="0 -2 24 22" component={GraphicSvgIcon} />
));
