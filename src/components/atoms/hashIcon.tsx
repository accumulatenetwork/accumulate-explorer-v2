import React from 'react';
import { SvgIcon } from '@mui/material';
import HashSvgIcon from '@/assets/hash.svg';

export const HashIcon = React.memo((props) => (
  <SvgIcon {...props} viewBox="0 0 16 16" component={HashSvgIcon} />
));
