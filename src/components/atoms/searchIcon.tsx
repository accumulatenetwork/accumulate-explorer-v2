import React from 'react';
import { SvgIcon } from '@mui/material';
import SearchSvgIcon from '@/assets/search.svg';

export const SearchIcon = React.memo((props) => (
  <SvgIcon {...props} viewBox="-2 -2 24 24" component={SearchSvgIcon} />
));
