import React from 'react';
import NextLink, { LinkProps as NextLinkProps } from 'next/link';
import { Link as MuiLink, LinkProps as MuiLinkProps } from '@mui/material';
import { styled } from '@mui/material/styles';

// `LinkProps` is the combination of the MUI `LinkProps` and the Next `LinkProps`
// We wanna use the `href` prop from `next/link` so we omit it from MUI's.
export type LinkProps = Omit<MuiLinkProps, 'href'> &
  Omit<NextLinkProps, 'as' | 'passHref' | 'children'>;

export const Link = styled(
  React.forwardRef<HTMLAnchorElement, LinkProps>(function Link(
    { href, prefetch, replace, scroll, shallow, locale, ...muiProps },
    ref,
  ) {
    return (
      <NextLink
        href={href}
        replace={replace}
        scroll={scroll}
        shallow={shallow}
        locale={locale}
        passHref
      >
        <MuiLink ref={ref} {...muiProps} />
      </NextLink>
    );
  }),
)<LinkProps>``;
