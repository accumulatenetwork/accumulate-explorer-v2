import React from 'react';
import { SvgIcon } from '@mui/material';
import OppositeArrows from '@/assets/oppositeArrows.svg';

export const OppositeArrowsIcon = React.memo((props) => (
  <SvgIcon {...props} viewBox="0 -4 24 24" component={OppositeArrows} />
));
