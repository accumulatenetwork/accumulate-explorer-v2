import React from 'react';
import { SvgIcon } from '@mui/material';
import roundedArrowRightSvgIcon from '@/assets/roundedArrowRight.svg';

export const RoundedArrowRightIcon = React.memo((props) => (
  <SvgIcon
    {...props}
    viewBox="0 0 26 27"
    component={roundedArrowRightSvgIcon}
  />
));
