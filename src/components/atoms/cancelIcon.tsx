import React from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';
import CancelSvgIcon from '@/assets/cancel.svg';
import { styled } from '@mui/material/styles';

export const CancelIcon = styled(
  React.memo((props: SvgIconProps) => (
    <SvgIcon {...props} viewBox="0 0 10 10" component={CancelSvgIcon} />
  )),
)`
  width: 10px;
  height: 10px;
`;
