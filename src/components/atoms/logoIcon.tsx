import { SvgIcon, SvgIconProps } from '@mui/material';
import { styled } from '@mui/material/styles';
import SearchLogoIcon from '@/assets/logo.svg';

export const LogoIcon = styled((props: SvgIconProps) => (
  <SvgIcon {...props} viewBox="0 0 44 45" component={SearchLogoIcon} />
))`
  width: 44px;
  height: 45px;
  display: inline-block;
`;
