import ButtonUnstyled from '@mui/base/ButtonUnstyled';
import { styled } from '@mui/material/styles';

export const IconHideButton = styled(ButtonUnstyled)`
  padding: 0;
  font-size: 24px;
  background: none;
  border: none;
  cursor: pointer;
  color: ${(props) => props.inputColor || '#00A6FB80'};
  visibility: ${(props) => (props.hidden ? 'hidden' : 'visible')};
  opacity: 0.5;
  height: 24px;
  width: 24px;
  align-self: center;
  border-radius: 50%;
  overflow: hidden;
  transition: all 200ms;

  &:hover {
    opacity: 1;
  }
`;
