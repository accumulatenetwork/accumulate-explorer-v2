import { Container, Grid } from '@mui/material';
import { TopValidators } from '@/components/organisms/topValidators';
import { HistoryLinearTransaction } from '@/components/organisms/historyLinearTransaction';
import { RecentBlocks } from '@/components/organisms/recentBlocks';
import { RecentTransactions } from '@/components/organisms/recentTransactions';

export const Metrics = () => {
  return (
    <Container maxWidth="xl">
      <Grid container spacing={2}>
        <Grid item xs={6}>
          <TopValidators />
        </Grid>
        <Grid item xs={6}>
          <HistoryLinearTransaction />
        </Grid>
        <Grid item xs={6}>
          <RecentBlocks />
        </Grid>
        <Grid item xs={6}>
          <RecentTransactions />
        </Grid>
      </Grid>
    </Container>
  );
};
