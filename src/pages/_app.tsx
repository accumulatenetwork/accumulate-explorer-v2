import App, { AppInitialProps } from 'next/app';
import Head from 'next/head';
import Router from 'next/router';

import { ToastContainer } from 'react-toastify';
import { ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { CacheProvider } from '@emotion/react';

import { theme } from '@/theme';
import createEmotionCache from '../utils/createCache';

import { Header } from '@/components/organisms/header';
import { GlobalSearch } from '@/components/organisms/globalSearch';
import { withGlobalSearch } from '@/containers/globalSearch';
import { withRouter } from '@/containers/router';
import { withToast } from '@/containers/toast';

import { Provider } from 'react-redux';
import { store } from '../store';

import 'react-toastify/dist/ReactToastify.css';
import '../styles/index.scss';

const clientSideEmotionCache = createEmotionCache();

export interface initProps extends AppInitialProps {
  emotionCache: any;
}

const WrappedGlobalSearch = withToast(
  withRouter(withGlobalSearch(GlobalSearch)),
);

export default class UIApp extends App<initProps> {
  static async getInitialProps({ Component, ctx }) {
    const props = {
      pageProps: {
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
      },
    };

    return props;
  }

  componentDidMount(): void {
    // Remove the server-side injected CSS.
    const jssStyles: HTMLElement = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles);
    }

    const { pathname } = Router;

    if (pathname == '/') {
      Router.push('/dashboard');
    }
  }

  render() {
    const {
      Component,
      pageProps,
      router,
      emotionCache = clientSideEmotionCache,
    } = this.props;

    return (
      <CacheProvider value={emotionCache}>
        <Head>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
          {/*           <link
            href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap"
            rel="stylesheet"
          /> */}
        </Head>
        <CssBaseline />
        <Provider store={store}>
          <ThemeProvider theme={theme}>
            <>
              <Header />
              <WrappedGlobalSearch />
              <Component {...pageProps} key={router.route} />
              <ToastContainer
                position="top-center"
                autoClose={false}
                hideProgressBar={true}
                newestOnTop={false}
                draggable={false}
                closeOnClick={true}
              />
            </>
          </ThemeProvider>
        </Provider>
      </CacheProvider>
    );
  }
}
