import React from 'react';
import { Page } from '@/components/atoms/page';
import { Metrics } from '@/components/templates/metrics';
import { TitleBlock } from '@/components/organisms/titleBlock';
import { TransactionNumMetric } from '@/components/molecules/transactionNumMetric';

export const DashboardPage: React.FC = () => {
  return (
    <Page>
      <TitleBlock title="Dashboard">
        <TransactionNumMetric amount="0.00" />
      </TitleBlock>
      <Metrics />
    </Page>
  );
};

export default DashboardPage;
