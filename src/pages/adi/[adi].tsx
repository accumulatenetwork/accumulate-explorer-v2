import { useEffect, useState } from 'react';
import { Container, Skeleton } from '@mui/material';

import { Page } from '@/components/atoms/page';
import { TitleBlock } from '@/components/organisms/titleBlock';
import { AdiBlock } from '@/components/organisms/adiBlock';
import { AdiBlockContext } from '@/components/molecules/AdiBlockContext';
import { AdiBlockContextSkeleton } from '@/components/molecules/AdiBlockContextSkeleton';
import { CopyTitle } from '@/components/organisms/CopyTitle';
import { withADIBlock } from '@/containers/adiBlock';

import { navigateTo } from '@/utils/index';

export const ADI = ({ adi: { data, pending, fetchData, resetData } }) => {
  const [rows, setRows] = useState([]);
  const skeletonRows = new Array(3).fill({ height: 42 });

  useEffect(() => {
    fetchData();
    return () => resetData();
  }, []);

  useEffect(() => {
    if (!data) {
      return;
    }

    setRows([
      {
        title: 'ADI URL',
        text: data.text,
        isTextCopy: true,
        hint: 'some tooltip info',
        hash: data.textHash,
      },
      {
        title: 'Type',
        text: data.type,
      },
      {
        title: 'Key Book',
        text: data.keyBook,
        hash: data.keyBookHash,
        isTextCopy: true,
        link: navigateTo('keybook', data.keyBook),
      },
    ]);
  }, [data]);

  return (
    <Page>
      <TitleBlock title="ADI" justify="flex-start">
        {!data || pending ? (
          <Skeleton variant="rectangular" width={140} height={30} />
        ) : (
          <CopyTitle title={data.url} />
        )}
      </TitleBlock>
      <Container maxWidth="xl">
        <AdiBlock>
          {!data || pending ? (
            <AdiBlockContextSkeleton rows={skeletonRows} />
          ) : (
            <AdiBlockContext rows={rows} />
          )}
        </AdiBlock>
      </Container>
    </Page>
  );
};

export default withADIBlock(ADI);
