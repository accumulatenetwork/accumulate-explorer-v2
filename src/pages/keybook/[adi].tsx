import { useEffect } from 'react';

import { Container, Skeleton } from '@mui/material';

import { Page } from '@/components/atoms/page';
import { TitleBlock } from '@/components/organisms/titleBlock';
import { CopyTitle } from '@/components/organisms/CopyTitle';
import { withADIBlock } from '@/containers/adiBlock';

export const KeyPage = ({ adi: { data, pending, fetchData, resetData } }) => {
  useEffect(() => {
    fetchData();
    return () => resetData();
  }, []);

  return (
    <Page>
      <TitleBlock title="Key book" justify="flex-start">
        {!data || pending ? (
          <Skeleton variant="rectangular" width={140} height={30} />
        ) : (
          <CopyTitle title={data.url} />
        )}
      </TitleBlock>
      <Container maxWidth="xl">{/* <AdiBlock data={data} /> */}</Container>
    </Page>
  );
};

export default withADIBlock(KeyPage);
