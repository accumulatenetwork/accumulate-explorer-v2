import { ReactNode } from 'react';

export type Any = { [k: string]: any };

export interface Children {
  children?: ReactNode;
}
