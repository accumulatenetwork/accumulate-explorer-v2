export const colors = {
  black: '#000',
  white: '#fff',
  errorRed: '#e24242',
  dark: ['#051218'],
};
