import * as styles from '@mui/material/styles';
declare module '@mui/material/styles' {
  interface TypographyVariants {
    link: React.CSSProperties;
    listText: React.CSSProperties;
    hint: React.CSSProperties;
  }
  interface TypographyVariantsOptions {
    link?: React.CSSProperties;
    listText?: React.CSSProperties;
    hint?: React.CSSProperties;
  }

  interface PaletteOptions {
    black?: PaletteColorOptions;
  }

  interface Palette {
    black: PaletteColor;
  }
}

declare module '@mui/material/Typography' {
  interface TypographyPropsVariantOverrides {
    listText: true;
    link: true;
    hint: true;
  }
}
