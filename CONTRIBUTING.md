# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue,
email, or any other method with the owners of this repository before making a change.

Please note we have a code of conduct, please follow it in all your interactions with the project.

## GitFlow

+ Our main branch is `develop`
+ `main` is for latest stable release version
+ Branches for developers must follow the [commits type](#commits)

We're using GitFlow v2.

<img src="./public/Gitflow-Workflow-2.png" alt="GitFlow"/>

## Commits

In general, the majority of commits should be between 100 and 1000 total lines
changed (additions + deletions). A large number of small commits may indicate
that the author is breaking up a large change into many small commits, which can
make it harder to follow the changes. Very large changes should be broken up
into smaller commits. There are valid cases where small commits are OK, such as
fixing typos or bugs.

Commit messages should be of the form `<type>[scope]: <description>` where the
scope is optional and must be parenthesized ([conventional commits][1]). If the
commit cannot be fully described in one line, add a blank line followed by a
longer description. For example:

+ Simple

  ```
  feat: add token issuers
  ```

+ With scope

  ```
  feat(api): add token issuers
  ```

+ With additional details

  ```
  feat: add token issuers

  Add an API call to create a new token issuer.
  ```

The commit type should be one of the following:

| Type       | Description
| ---------- | -----------
| `EXP\-\d+` | New Jira features
| `feat`     | New features
| `fix`      | Bug fixes
| `chore`    | Chores, such as cleanup
| `perf`     | Performance improvements
| `ci`       | Changes relating exclusively to CI/CD
| `docs`     | Documentation
| `test`     | Changes relating exclusively to testing
| `revert`   | Reverting changes

Based on [this list][2]. Refactoring and style changes fall under `chore`.

[1]: https://www.conventionalcommits.org/en/v1.0.0/
[2]:Lhttps://github.com/conventional-changelog/commitlint/tree/master/%40commitlint/config-conventional#problems

## Naming convention

Use `UPPERCASE` for `CONSTANTS` only. 

Top-level entities, classes, protocols, categories, constructors, types - `UpperCamelCase`.

Variables, parameters, methods, names, functions - `lowerCamelCase`.

Function names should be verbs if the function changes the state of the program, and nouns if 
they're used to return a certain value.

## Submitting Changes

+ Just before pull request make sure you integrated latest changes from `develop` branch
+ When you are ready to submit your work, create a Pull Request on GitLab
  merging your branch into `develop`. If you are a core team member, your branch
  name must be or begin with the Jira task. For example,
  `EXP-5-search-adi-via-block-explorer`.
+ The pull request description must clearly describe the changes made. If the
  changes affect functionality, the description must include how the changes can
  be verified. Otherwise, the description must state that the changes do not
  affect functionality.

## Reviewing Changes

+ In general, a pull request should receive at least some constructive
  criticism, in the form of a requested change - everyone has room for
  improvement.
+ **The most important aspects to consider when reviewing changes are
  readability and verification.**
+ A pull request must be small enough that it can be reasonably reviewed. If the
  request is too large, ask the author to split the changes into multiple pull
  requests.
+ If the code is difficult to read, leave a comment. This could be a request to
  add additional comments, or to rewrite a function in a more readable way.
+ If the code is not verified or verifiable, leave a comment. This could be a
  request to add unit tests, or to rewrite a function in a more testable way, or
  it could be a comment that the reviewer was unable to debug the changes.

