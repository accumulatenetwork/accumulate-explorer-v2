# Accumulate Block Explorer v2

Next.js React App

## Code style

### Components:

- All components must avoid any business logic, only data transferring via public interface is allowed
- Components are divided in few categories according to Atomic Design technology (https://bradfrost.com/blog/post/atomic-web-design/)
- Private components are allowed under folder of main component
- Always try to wrap all components into styled decorator to allow using of it in styled-css as selector

### Containers (aka HOC):

This kind of components are used to fetch data on page level (getInitialProps) and pass this data to component

- Containers cannot edit data, only read it
- Containers are allowed to use only on page components

### Hooks:

- Hooks should be atomic as possible
- Hooks can fetch and edit data
- Single value can be returned from hook
- From 2 and up to 3 elements should be returned from hook as array, but only if ordering always the same. In all other cases use hash-maps (aka objects)

### Actions/Reducer/Selectors:

Folder structure of specific Actions/Reducer/Selectors should be placed:
/features/{subject}
/
  actions.ts
  reducer.ts
  selectors.ts

### General:

- Default exports in all files excluding pages are banned
- Interfaces of general types should be in interface folder
- Interfaces of props/data arguments/return types can be in same file/folder with regular function, but only if it's not a general type

## Requirements

Node.js v14.18+, NPM v6.0+, Docker 19.0+

## Setup and run

Project use npm, therefore we have package-lock.json for specifying dependencies.

Install dependencies by command `npm ci`

Start dev by command `npm run dev`

Build project by command `npm run build`

Start builded project by command `npm run start`
