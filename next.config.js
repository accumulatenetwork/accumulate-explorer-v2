const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});
const path = require('path');

const withTM = require('next-transpile-modules')([
  '@mui/material',
  '@mui/system',
  '@mui/icons-material',
]);

module.exports = withBundleAnalyzer(
  withTM({
    reactStrictMode: true,
    trailingSlash: true,
    webpack: (config) => {
      config.module.rules.push({
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      });

      config.resolve.alias['react'] = path.resolve(
        __dirname,
        'node_modules',
        'react',
      );
      config.resolve.alias['react-dom'] = path.resolve(
        __dirname,
        'node_modules',
        'react-dom',
      );

      return config;
    },
  }),
);
