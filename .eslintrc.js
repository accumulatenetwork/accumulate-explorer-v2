module.exports = {
  settings: {
    react: { version: 'detect' },
  },
  env: {
    browser: true,
    es6: true,
    node: true,
  },
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:react/recommended',
    'plugin:react-hooks/recommended',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
  },
  plugins: [
    'eslint-plugin-import',
    'eslint-plugin-react',
    'eslint-plugin-jsdoc',
    'eslint-plugin-prefer-arrow',
    '@typescript-eslint',
    '@typescript-eslint/tslint',
  ],
  rules: {
    'react/prop-types': 0,
    // temporary
    'react/display-name': 0,
    '@typescript-eslint/explicit-module-boundary-types': 0,
    'react/react-in-jsx-scope': 0,
    // Off due to same rule in @typescript-eslint/tslint/config
    '@typescript-eslint/no-empty-function': 0,
    '@typescript-eslint/tslint/config': [
      'warn',
      {
        lintFile: './tslint.json',
      },
    ],
  },
};
