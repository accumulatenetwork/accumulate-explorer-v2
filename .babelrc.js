module.exports = {
  presets: [
    [
      'next/babel',
      {
        'preset-env': {
          corejs: 3,
          exclude: ['@babel/plugin-transform-regenerator'],
          targets: {
            browsers:
              'last 1 chrome versions, last 1 firefox versions, last 1 safari version, last 1 edge version',
          },
          useBuiltIns: 'usage',
        },
      },
    ],
  ],
  plugins: [
    [
      '@emotion',
      {
        importMap: {
          '@mui/system': {
            styled: {
              canonicalImport: ['@emotion/styled', 'default'],
              styledBaseImport: ['@mui/system', 'styled'],
            },
          },
          '@mui/material/styles': {
            styled: {
              canonicalImport: ['@emotion/styled', 'default'],
              styledBaseImport: ['@mui/material/styles', 'styled'],
            },
          },
        },
      },
    ],
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-proposal-nullish-coalescing-operator',
    [
      'babel-plugin-transform-react-remove-prop-types',
      {
        removeImport: true,
        mode: 'remove',
      },
    ],
    'babel-plugin-transform-react-pure-class-to-function',
    [
      'babel-plugin-import',
      {
        libraryName: '@mui/material',
        libraryDirectory: '',
        camel2DashComponentName: false,
      },
      'core',
    ],
    [
      'babel-plugin-import',
      {
        libraryName: '@mui/icons-material',
        libraryDirectory: '',
        camel2DashComponentName: false,
      },
      'icons',
    ],
    'babel-plugin-polished',
  ].filter(Boolean),
};
