terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.70"
    }
  }

  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region = "eu-central-1"
}

#resource "aws_dynamodb_table" "terraform-state" {
#  name           = "terraform-state-security"
#  read_capacity  = 20
#  write_capacity = 20
#  hash_key       = "LockID"
#
#  attribute {
#    name = "LockID"
#    type = "S"
#  }
#}

data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-arm64-gp2"]

  }
}

#resource "aws_key_pair" "deployer" {
#  key_name   = "deployer-key-dev"
#  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC3TcDDShqtTfiWBj4xltz0U7QdrW6vAc7raDcLug8VFjxcavBLR9/xLPMcPh8P7GpvrR9oGdHP4jDoNtOIKxvNpGBx9WEAwacv+jft3syf3IpMkWZXj2g904NyYVSotWAtXx1T6ffpy1+K51ArQBLM83Bf95m8a1lCB3UsVd5UBHECjQ71FYu3dAfcWzQnZyA5PSa2TkRQSHe2+jUrjhph/3wFibqZmCyRtnECCTv6lb/vCwZpQNse3KD1mV/cTCla4DIkVnw2K2q9I+bL6izrHeJfNcYKkKU7HRsnuCPyUYzvkSWCSS3Com5fu1OWYQJMOLJBih6VAwBgqjRG/V1oj+T42lFvYAJcmFxvXJbREKV0ag476XItug3iCDBEO8sbd7FGB0ERi1UgqxRixqcG4uxMZ1oFXLmvI8qjdxhBaGIygXmnB36JRYGAfMiDqiXIHZQn+4fgrr9dcNyCsQ4cfAUeo73birwKI+XmbFHVoiutKWDZjR8RZEaBqLXKD/E= root@v-hlushenok.do.local"
#}

module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "Gitlab_runner"

  ami                    = "ami-0979d16bc6b1b6d87"
  instance_type          = "t4g.small"
  key_name               = "deployer-key-dev-pro"
  monitoring             = true
  vpc_security_group_ids = [aws_security_group.gitlab.id]
  subnet_id              = "subnet-1ec1ed54"
  user_data = file("user_data.sh")

  tags = {
    Terraform   = "true"
    #Environment = "dev"
    Project = "Gitlab"
    CostGroup = "DevPro"
  }
}


resource "aws_security_group" "gitlab" {
  egress = [
    {
      cidr_blocks      = ["0.0.0.0/0", ]
      description      = ""
      from_port        = 0
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "-1"
      security_groups  = []
      self             = false
      to_port          = 0
    }
  ]
  ingress = [
    {
      cidr_blocks      = ["0.0.0.0/0", ]
      description      = ""
      from_port        = 22
      ipv6_cidr_blocks = []
      prefix_list_ids  = []
      protocol         = "tcp"
      security_groups  = []
      self             = false
      to_port          = 22
    }
  ]
  tags = {
    Terraform   = "true"
    #Environment = "dev"
    Project = "Gitlab"
    CostGroup = "DevPro"
  }
}

#resource "aws_eip" "lb" {
#  vpc      = true
#  instance = module.ec2_instance.id
#  tags = {
#    Terraform   = "true"
#    Environment = "dev"
#    Project = "BlockExplorer"
#    CostGroup = "DevPro"
#  }
#}

#output "instance_ip_addr" {
#  value = aws_eip.lb.public_ip
#}

