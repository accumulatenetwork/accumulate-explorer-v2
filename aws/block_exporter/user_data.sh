#! /bin/bash
sudo yum update -y


# install epel
amazon-linux-extras install epel -y

amzn2-core

# install fail2ban
sudo yum -y install fail2ban
sudo systemctl enable fail2ban
sudo systemctl start fail2ban


# install docker, docker-compose
sudo yum install docker -y
wget https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m)
sudo mv docker-compose-$(uname -s)-$(uname -m) /usr/bin/docker-compose
sudo chmod -v +x /usr/bin/docker-compose
sudo systemctl enable docker.service
sudo systemctl start docker.service


# install apache
#sudo yum install -y httpd
#sudo systemctl start httpd
#sudo systemctl enable httpd
#echo "The page was created by the user data" | sudo tee /var/www/html/index.html

# create new users
for user_name in "vhlushenok" "pfomenko" "mshteinikov" "gitlab"
do
  sudo adduser $user_name
  sudo mkdir /home/$user_name/.ssh
  sudo chmod 700 /home/$user_name/.ssh
  sudo touch /home/$user_name/.ssh/authorized_keys
  sudo chmod 600 /home/$user_name/.ssh/authorized_keys
  sudo chown -R $user_name:$user_name /home/$user_name/.ssh
  sudo usermod -a -G wheel $user_name
  sudo sed -i /etc/sudoers -re 's/^%wheel.*/%wheel ALL=(ALL) NOPASSWD: ALL/g'
done

sudo echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCsus/6J5febB1JTT7OFJHYZHbr+N+MtKNb/y4k9YH5wNP4psUE6FQwuoJ6MzSDpnLXQLO3POzTmdYz2J9wfHphmDRTsztMQayPJG3Depe16OYe+ck0ozBtL3rlg2MWFe3dd4K0/PoHylJsCrkM1lPXZDElZXtypBZc2v/SgXZe7vaBsegCA7LcXKmuo0IcoQJejtYKMEuh45O1kJL8K28byBsjoIuFp4ZhhxpUTX5NZ19Ff/Vj1J5rXpbTwkZjcp1U2oxGRBOixyV47MlGi6/KpJXb1EBxVRZuvNgCtqdmnxt6beMmSzFvd8ihOs0nzxipUIRA+pBBsHAc3oyqHxxzjXCYZFUoxfahFJJ4XcEhgV6uu5Az6ORl9orgsH6rdOTPw1n4V5J8UPisr/Doy2wjAEsyQxO9MFdLj1fYnEyydUvyPX/yXyb90gZUIz7/RcCyEFpLyp8YbXJsgHY35VIHKmPDMh1v0797yL4v//FaEknbZgPZsiJ3ifulpVYhEgM= root@v-hlushenok.do.local" > /home/vhlushenok/.ssh/authorized_keys
sudo echo "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB11ddG7Q+PRJDL2h/GS03z3qZKu3plwQ1ii2oM4tFP6 pavlo.fomenko@dev.pro" > /home/pfomenko/.ssh/authorized_keys
sudo echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCozOYfUQaAzWmyWJv30MsBHt2Mp5eOs1BhaBaB0zWy6o/2uGUhhg5uczQ6zu/PDr0xMcAdaRpBLHy8kdLJHgG00r7Vi674hS1DN/2121A8Gc2tIpT6Z80SIi79mL7W3VqE5Cg0Bo1MYxjYjBf1GaMOWpWmoLIfy7T6q+NkZp8nymOyyqQGxLXvxghEfwXKKdgb+SBaBXIUePhtptW7KTkY4h/lqdWWxn3KNjlvnZ+ovzMSvdPCKwe/kOJJeINVuke77F0fIwgegYF4LPL2XVqw9I3h8Y7JspW4hp/fvVXP2ULdnXHsj+Uj6Zo25rTj/Art95oDgFVD3nNuaDto09/H mykyta.shteinikov@m-shteinikov.local" > /home/mshteinikov/.ssh/authorized_keys
#sudo echo "" > /home/gitlab/.ssh/authorized_keys


